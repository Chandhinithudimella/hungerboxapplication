package com.hungerbox.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ResponseDto {
	private String message;
	
	
	private String employeeName;
	
	private String email;
	
	private Double orderAmount;
	
	private String phone;
	
	private LocalDateTime orderPlacedAt;
	
	
	
	
	

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getOrderPlacedAt() {
		return orderPlacedAt;
	}

	public void setOrderPlacedAt(LocalDateTime orderPlacedAt) {
		this.orderPlacedAt = orderPlacedAt;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
