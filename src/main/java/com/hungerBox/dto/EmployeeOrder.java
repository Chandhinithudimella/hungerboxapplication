package com.hungerbox.dto;

import java.util.List;

import com.hungerbox.model.Employee;
import com.hungerbox.model.OrderItemList;

public class EmployeeOrder {

	private Employee employee;
	private List<OrderItemList> orders;
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public List<OrderItemList> getOrders() {
		return orders;
	}
	public void setOrders(List<OrderItemList> orders) {
		this.orders = orders;
	}
}
