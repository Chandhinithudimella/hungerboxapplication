package com.hungerbox.service;

import java.util.List;
import com.hungerbox.model.Item;

public interface SearchService {

	List<Item> getItemByNameStartingWith(String name);

}
